#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "hardware/gpio.h"
#include "hardware/watchdog.h"

#define BUTTON_GP21_PIN 21
#define RGB_LED_RED_PIN 0
#define RGB_LED_GREEN_PIN 0
#define RGB_LED_BLUE_PIN 0

// Declare the main assembly entry point before use.
void main_asm();

// Button event detection handler and Morse code input buffer logic
// should be implemented in ARM assembly

void display_welcome_message() {
    // Display the welcome message and instructions on the UART console
}

void display_level_menu() {
    // Display the level menu and instructions on the UART console
}

int get_player_level_selection() {
    // Retrieve the player's level selection from the Morse code input buffer
    // and return the selected level number
}

void display_current_level(int level) {
    // Display the current level and its instructions on the UART console
}

void update_game_state(int level, int lives) {
    // Update the game state based on the level and number of lives remaining
}

void game_logic() {
    display_welcome_message();

    while (true) {
        display_level_menu();
        int level = get_player_level_selection();
        display_current_level(level);

        int lives = 3;
        int correct_sequences_in_a_row = 0;

        while (lives > 0) {
            // Present the player with an alphanumeric character
            // Retrieve the player's Morse code input
            // Check the player's input against the expected Morse code sequence
            // Update the player's lives and RGB LED color
            // Handle level progression and game completion

            update_game_state(level, lives);
        }
    }
}

/*
 * Main entry point for the code - simply calls the main assembly function.
 */
int main() {
    main_asm();
    return(0);
}
